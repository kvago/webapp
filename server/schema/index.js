import { mergeStrings } from 'gql-merge';
import { makeExecutableSchema } from 'graphql-tools';
import { Data, BinancePayload } from '../types/Data';
import { User } from '../types/User';
import { Trade } from '../types/Trade';
import { SigninPayload } from '../types/SigninPayload';

import resolvers from '../resolvers';

 // TODO: remove all User data from findUserByEmail

const typeDefs = `
type Mutation {
  createUser(name: String!, registrationDate: Float, authProvider: AuthProviderSignupData!): User,
  saveTradePair(mainCurrency: String!, secondaryCurrency: String!, stock: String, leftPrice: Float, rightPrice: Float): Trade,
  editTradePair(id: String!, mainCurrency: String!, secondaryCurrency: String!, leftPrice: Float, rightPrice: Float): Trade,
  deleteTradePair(id: String!): Trade,
  signInUser(email: AUTH_PROVIDER_EMAIL): SigninPayload!
}
type Query {
  userProfile (token: String): User,
  userTradePairs: [Trade],
  findUserByEmail (email: String!): User
}
input AuthProviderSignupData {
    email: AUTH_PROVIDER_EMAIL
}
input AUTH_PROVIDER_EMAIL {
    email: String!
    password: String!
}
`;

const resultSchema = mergeStrings([User, Data, Trade, BinancePayload, SigninPayload, typeDefs]);

module.exports = makeExecutableSchema({typeDefs: resultSchema, resolvers});
