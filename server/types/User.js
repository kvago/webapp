export const User = `
type User {
    id: ID!  @unique
    name: String!
    telegramm: String
    email: String! @unique
    registrationDate: Float
    lastAuthDate: String
    confirmEmail: Boolean
    confirmTelegramm: Boolean
    notifications: [Channels]
    tradeId: String
}

enum Channels {
    telegramm
    email
}

`
