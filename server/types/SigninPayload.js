export const SigninPayload = `
type SigninPayload {
    message: String
    status: Int!
    data: Data
}
`