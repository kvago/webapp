export const Trade = `
type Trade {
    id: ID!  @unique
    userId: String!
    timestamp: Float
    mainCurrency: String
    secondaryCurrency: String
    leftPrice: Float
    rightPrice: Float
    stock: [Stocks]
    confirmTelegramm: Boolean
}

enum Stocks {
    BINANCE
    BITTREX
}
`
