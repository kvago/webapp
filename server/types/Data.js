export const Data = `
type Data {
    token: String
    user: User
}
`

export const BinancePayload = `
type BinancePayload {
    symbol: String!
    price: String!
}
`