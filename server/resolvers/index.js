import Common from '../src/common';
import updateUserTime from '../src/user-last-time-login'
const ObjectId = require('mongodb').ObjectId;

module.exports = {
  Query: {
    userProfile: async (root, { token }, { mongo: { Users }, user }) => {
      if (!token || !user) {
        return;
      }

      const decoded = await Common.decode(token);

      if (ObjectId(user._id).toString() === decoded.id) {
        await updateUserTime(Users, ObjectId(user._id))
        return user;
      }

      throw new Error('User not found');
    },
    userTradePairs: async (root, data, {mongo: {Trade}, user}) => {
      if (!user) {
        throw new Error('You must be authorized');
      }
      return await Trade.find({userId: ObjectId(user._id)}).toArray();
    },
    findUserByEmail: async (root, {email}, {mongo: {Users}}) => {
      return await Users.findOne({email});
    },
  },

  Mutation: {
    createUser: async (root, data, {mongo: {Users}}) => {
      const savedUsers = await Users.find({email: data.authProvider.email.email}).toArray();
      if (savedUsers.length > 0) {
        throw new Error('User with this email allready created');
      }
      const newUser = {
          name: data.name,
          email: data.authProvider.email.email,
          password: Common.encrypt(data.authProvider.email.password),
          registrationDate: Date.now(),
          lastAuthDate: Date.now(),
          confirmEmail: false,
          confirmTelegramm: false,
          notifications: []
      };
      const response = await Users.insert(newUser);
      return Object.assign({id: response.insertedIds[0]}, newUser);
    },
    editTradePair: async (root, data, { mongo: { Trade }, user }) => {
      const { id, mainCurrency, secondaryCurrency, leftPrice, rightPrice } = data;

      if (!user) {
        throw new Error('You have no permission for that type operation');
      }

      if (!id) {
        throw new Error('No Trade pair ID');
      }

      if (!mainCurrency || !secondaryCurrency || !leftPrice || !rightPrice) {
        throw new Error('Invalid Trade update query parametrs');
      }

      const response = await Trade.updateOne(
        { _id : ObjectId(id)},
        { $set: {
            mainCurrency,
            secondaryCurrency,
            leftPrice,
            timestamp: Date.now(),
            rightPrice,
          }
        }
      )

      if (response.modifiedCount === 0) {
        throw new Error('Error while updating document');
      }

      return {id, timestamp: Date.now(), mainCurrency, secondaryCurrency, leftPrice, rightPrice};
    },
    deleteTradePair: async (root, data, { mongo: { Trade }, user }) => {
      const { id } = data;

      if (!user) {
        throw new Error('You have no permission for that type operation');
      }

      if (!id) {
        throw new Error('No Trade pair ID');
      }

      const response = await Trade.deleteOne({ _id : ObjectId(id)});

      if (response.deletedCount === 0) {
        throw new Error('Error while deleting document');
      }

      return null;
    },
    saveTradePair: async (root, data, { mongo: { Trade }, user }) => {
        const { mainCurrency, secondaryCurrency, stock, leftPrice, rightPrice } = data;

        if (!user) {
            throw new Error('No permission for that type operation');
        }

        if (!mainCurrency || !secondaryCurrency || !stock || !leftPrice || !rightPrice) {
            throw new Error('Invalid trade pair parametr value');
        }

        const newTradePair = {
            stock: 'BINANCE',
            mainCurrency,
            userId: user._id,
            secondaryCurrency,
            leftPrice,
            timestamp: Date.now(),
            rightPrice,
            confirmTelegramm: false
        };
        const response = await Trade.insert(newTradePair);
        return Object.assign({id: response.insertedIds[0]}, newTradePair);
    },
    signInUser: async (root, data, {mongo: {Users}}) => {
      const user = await Users.findOne({email: data.email.email});
      if (!user) {
        return {message: "No user with this email", status: 404};
      }
      if (data.email.password === Common.decrypt(user.password)) {
        await updateUserTime(Users, ObjectId(user._id))
        return {
          data: {
            token: Common.encode({username: user.name, id: user._id}),
            user
          },
          message: "Success",
          status: 200
        };
      } else {
        return {message: "wrong password", status: 403}
      }
    },
  },

  User: {
    id: root => root._id || root.id,
  },

  Trade: {
    id: root => root._id || root.id,
  },
};
