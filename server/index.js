import path from 'path'
import express from 'express';
import { graphqlExpress, graphiqlExpress } from 'graphql-server-express';
import WebSocket from 'ws';
import bodyParser from 'body-parser';
import cors from 'cors';
import morgan from 'morgan';
import config from './src/config';

// User Authenticate
const { authenticate } = require('./src/authentication');

const schema = require('./schema');
const connectMongo = require('./src/mongo-connector');

const PORT = config.port;
const WS_PORT = config.ws;

const wss = new WebSocket.Server({ port: WS_PORT });

wss.on('connection', (ws) => {
    const interval = setInterval(() => {
        ws.send('ping');
    }, 7000);
    ws.on('message', (message) => {
        console.log('received: %s', message);
    });
    ws.send('u are connected');
    ws.on('close', (code) => {
        clearInterval(interval);
        console.log('Status: connection closed code: %s', code)
    });
});

const start = async () => {
  const mongo = await connectMongo();
  const app = express();
  app.use(morgan('dev'));
  app.use('*', cors({ origin: 'http://localhost:3000' }));
  app.use(express.static(path.join(__dirname, '../client/build')));
  app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, '../client/build', 'index.html'));
  });
  const buildOptions = async (req, res) => {
    const user = await authenticate(req, mongo.Users);
    return {
      context: { mongo, user }, // This context object is passed to all resolvers.
      schema,
    };
  };
  app.use('/graphql', bodyParser.json(), graphqlExpress(buildOptions));

  app.use('/graphiql', graphiqlExpress({
    endpointURL: '/graphql'
  }));

  app.listen(PORT, () => {
    console.log(`Hackernews GraphQL server running on port ${PORT}.`)
  });
};

start().catch(error => console.error(error.stack));
