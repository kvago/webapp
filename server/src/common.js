import crypto from 'crypto';
import Jwt from 'jsonwebtoken';
import config from './config';

const algorithm = 'aes-256-ctr';
const JWT_SECRET = config.auth.jwt.secret; // TODO: need different keys for password and token encrypt?
const PASSWORD_SALT = config.auth.passwordSalt;

const decrypt = (password) => {
    const decipher = crypto.createDecipher(algorithm, PASSWORD_SALT);
    let dec = decipher.update(password, 'hex', 'utf8');
    dec += decipher.final('utf8');
    return dec;
}

const encrypt = (password) => {
    const cipher = crypto.createCipher(algorithm, PASSWORD_SALT);
    let crypted = cipher.update(password.toString(), 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
}

const decode = async (token) => {
    try {
        return await Jwt.verify(token, JWT_SECRET);
    } catch (err) {
        // TODO: remove jwt check empty token, add error log
        return false;
    }
}

const encode = async ({username, id}) => {
    return Jwt.sign({username, id}, JWT_SECRET);
}

exports.decrypt = password => decrypt(password);
exports.decode = (token) => decode(token);
exports.encode = ({username, id}) => encode({username, id});
exports.encrypt = password => encrypt(password);
