module.exports = async (Users, ObjectId) => {
  return await Users.updateOne({ _id: ObjectId }, { $set: { "lastAuthDate": Date.now() } });
}
