import Common from './common';

const ObjectId = require('mongodb').ObjectId;

module.exports.authenticate = async ({ headers: { authorization } }, Users) => {
  const decoded = authorization && await Common.decode(authorization);
  return decoded && await Users.findOne({ _id: ObjectId(decoded.id) });
}
