import assert from 'assert'
import { decrypt, encrypt } from './common'

describe('Test token', () => {
  it('Values show be equal', async () => {
    const token = await encrypt('token')
    const text = await decrypt(token)
    assert.equal(text, 'token');
  });
});

