import config from './config';
import { MongoClient } from 'mongodb';

module.exports = async() => {
  const db = await MongoClient.connect(config.databaseUrl);
  return {
    Users: db.collection('users'),
    Trade: db.collection('trade')
  };
}
