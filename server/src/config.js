if (process.env.BROWSER) {
    throw new Error(
      'Do not import `config.js` from inside the client-side code.',
    );
  }

  module.exports = {
    // Node.js app
    port: process.env.PORT || 4000,
    ws: process.env.WS || 4043,
    // API Gateway
    api: {
      // API URL to be used in the client-side code
      clientUrl: process.env.API_CLIENT_URL || '',
      // API URL to be used in the server-side code
      serverUrl:
        process.env.API_SERVER_URL ||
        `http://localhost:${process.env.PORT || 3000}`,
    },

    // Database
    databaseUrl: process.env.DATABASE_URL || 'mongodb://kvago36:4ny3tv3671@redkreigtober-shard-00-00-9kwif.mongodb.net:27017,redkreigtober-shard-00-01-9kwif.mongodb.net:27017,redkreigtober-shard-00-02-9kwif.mongodb.net:27017/test?ssl=true&replicaSet=RedKreigtober-shard-0&authSource=admin',

    // Authentication
    auth: {
      jwt: {
        secret: process.env.JWT_SECRET || 'React Starter Kit',
      },
      passwordSalt: process.env.PASSWORD_SALT || 'React Starter Kit',
    },
  };
