import React from 'react';
import { graphql, compose } from 'react-apollo';
import { Link } from 'react-router-dom';
import gql from 'graphql-tag';

const Test = ({ loading, error, networkStatus }) => {
    if (loading) {
      return <div>{loading}</div>;
    }

    if (error) {
      return <div>{error}</div>;
    }
    return <div>{networkStatus.isConnected && 'True'}</div>
}

const UPDATE_NETWORK_STATUS = gql`
  mutation updateNetworkStatus($isConnected: Boolean) {
    updateNetworkStatus(isConnected: $isConnected) @client
  }
`;

const NETWORK_STATUS = gql`
  query {
    networkStatus @client {
      isConnected
    }
  }
`;

export default graphql(NETWORK_STATUS, {
    props: ({ data: { networkStatus, error } }) => ({
        error,
        networkStatus,
    }),
})(Test);

// export default graphql(NETWORK_STATUS, {
//   props: ({ data: { loading, error, networkStatus } }) => ({
//     loading,
//     error,
//     networkStatus,
//   }),
// })(Test);
