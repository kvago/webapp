import React from "react";
import { Route, Redirect } from "react-router-dom";

const PrivateRoute = ({ component: Component, user, ...rest }) => (
    <Route {...rest} render={(props) => (
        Object.keys(user).length > 0 ? <Component {...props}/> : <Redirect to={{
            pathname: '/login',
            state: {
                from: props.location,
                redirectTitle: 'Нет доступа',
                redirectMessage: 'Для просмотра этой страницы вы должны быть авторизованны'
            }}}
        />
        )}
    />
)

export default PrivateRoute
