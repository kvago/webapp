import React from 'react';
import { Link } from 'react-router-dom'
import { Form, Icon, Input, Button, Checkbox, message } from 'antd/lib/index';
import { graphql, compose } from 'react-apollo/index';
import { GC_AUTH_TOKEN, GC_USER_ID } from '../../constants';
import gql from 'graphql-tag';
import './Login.css';

const { Item: FormItem } = Form;

class Login extends React.Component {
  handleSubmit = (e) => {
    e.preventDefault();
    const sendLoginQuery = async (email, password) => {
        const result = await this.props.signInUserQuery({
          variables: {
            email,
            password
          }
        });

        if (result.data.signInUser.status === 404) {
            message.error('Пользователя с такой почтой нет');
            return;
        }

        if (result.data.signInUser.status === 403) {
            message.error('Неправильный пароль');
            return;
        }

        message.success('Добро пожаловать');
        const { name, id } = result.data.signInUser.data.user
        localStorage.setItem(GC_USER_ID, id);
        localStorage.setItem(GC_AUTH_TOKEN, result.data.signInUser.data.token);
        this.props.setUser({ id, name })
        this.props.history.push('/');
    }
    this.props.form.validateFields((err, values) => {
      if (!err) {
        sendLoginQuery(values.email, values.password);
        console.log('Received values of form: ', values);
      }
    });
}
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
        <div className="login_page">
          <Form autoComplete="off" onSubmit={this.handleSubmit} className="login-form">
            <FormItem>
              {getFieldDecorator('email', {
                rules: [{ required: true, message: 'Please input your username!' }],
              })(
                <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Почта" />
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator('password', {
                rules: [{ required: true, message: 'Please input your Password!' }],
              })(
                <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Пароль" />
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator('remember', {
                valuePropName: 'checked',
                initialValue: true,
              })(
                <Checkbox>Remember me</Checkbox>
              )}
              <Link to="/access" className="login-form-forgot">Forgot password</Link>
              <Button type="primary" htmlType="submit" className="login-form-button">
                Log in
              </Button>
              Or <Link to="/register">sign up</Link> now!
            </FormItem>
          </Form>
      </div>
    );
  }
}

const SIGN_IN_USER = gql`
mutation signInUserQuery($email: String!, $password: String!) {
  signInUser(email: {
    email: $email,
    password: $password
  }) {
    message
    status
    data {
      token
      user {
        id,
        name
      }
    }
  }
}
`

export default compose(
  Form.create(),
  graphql(SIGN_IN_USER, { name: 'signInUserQuery' })
)(Login);
