import React from 'react';
import Login from './Login'
import { UserContext } from '../../Context/UserContext/user-context'

const LoginWrapper = (props) => {
  return (
    <UserContext.Consumer>
      {
        (({ setUser }) => (
          <Login {...props} setUser={setUser} />
        ))
      }
    </UserContext.Consumer>
  );
}

export default LoginWrapper
