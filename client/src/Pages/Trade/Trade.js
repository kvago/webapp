import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Select, Button, Popconfirm, Input, Spin, Alert } from 'antd';
import moment from 'moment';
import './Trade.css';

const Option = Select.Option;

export default class Trade extends Component {
  static propTypes = {
    assets: PropTypes.object,
    baseAssets: PropTypes.array,
    pair: PropTypes.object,
    editing: PropTypes.bool
  };
  static defaultProps = {
      assets: {},
      object: {},
      editing: false,
      baseAssets: []
  };
  state = {
    baseValue: undefined,
    value: '',
    editing: false,
    loading: false,
    error: false,
    errorPlace: false,
    leftPrice: '',
    rightPrice: ''
  }
  onMainValue = (value) => {
    this.setState({
      baseValue: value,
      value: this.props.assets[value][0],
    });
  }
  onSecondValue = (value) => {
    this.setState({
      value: value,
    });
  }
  changeComponentMode = () => {
    this.setState({
      editing: !this.state.editing,
    });
  }
  saveTradeAlert = () => {
    const id = this.props.id;
    const { baseValue, value, leftPrice, rightPrice } = this.state;
    const data = {
      id,
      baseValue: baseValue || this.props.mainCurrency,
      value: value || this.props.secondaryCurrency,
      leftPrice: leftPrice || this.props.leftPrice,
      rightPrice: rightPrice || this.props.rightPrice
    }
    this.changeComponentMode();
    this.props.save(data);
  }
  changeBordersValue = (event) => {
    event.preventDefault();
    const { name, value } = event.target;
    const onlyFloatNumbers = /^[+-]?\d+(\.\d+)?$/;
    if (value === '') {
      this.setState({
        [name]: value,
        error: false,
        errorPlace: false
      });
      return;
    }
    if (!onlyFloatNumbers.test(value)) {
      this.setState({
        error: `Ошибка, введен не допустимый символ, разрешаются только цифры и '.'`,
        errorPlace: name,
      });
      return;
    }
    this.setState({
      [name]: value,
      error: false,
      errorPlace: false
    });
  }
  render() {
    const { baseValue, value, editing, loading, error, errorPlace, leftPrice, rightPrice } = this.state;
    const baseAssetsOptions = this.props.baseAssets.map(baseAsset => <Option key={baseAsset}>{baseAsset}</Option>);
    const assetsOptions = (this.state.baseValue || this.props.mainCurrency) && this.props.assets[this.state.baseValue || this.props.mainCurrency].map(asset => <Option key={asset}>{asset}</Option>);
    const disabled = (!leftPrice && !rightPrice) || (!baseValue || !value) || (error && true);
    if (loading) {
      return (
        <div className="trade__pair trade__pair_loading">
          <Spin size="large" />
        </div>
      );
    }
    if (this.props.editing && !editing) {
      const { timestamp, mainCurrency, secondaryCurrency, leftPrice, rightPrice } = this.props;
      return (
        <div className="trade__pair trade__pair_show">
          <Button icon="edit" onClick={this.changeComponentMode} />
          <div style={{marginLeft: '20px'}} className="trade__field trade__field_date">{moment(timestamp).format('DD:MM:YY, h:mm')}</div>
          <div className="trade__field trade__field_currency">{mainCurrency}</div>
          <div className="trade__field trade__field_currency">{secondaryCurrency}</div>
          <div style={{color: 'red'}} className="trade__field trade__field_price">{leftPrice}</div>
          <div style={{color: 'green'}} className="trade__field trade__field_price">{rightPrice}</div>
        </div>
      );
    }
    return (
      <div className="trade__pair trade__pair_editing">
        <Button icon="close" onClick={this.props.close || this.changeComponentMode} />
        <Select className="trade_pair" value={this.state.baseValue || this.props.mainCurrency} style={{ width: 120 }} onChange={this.onMainValue}>
          {baseAssetsOptions}
        </Select>
        <Select className="trade_pair" disabled={!this.state.baseValue && !this.props.mainCurrency} value={this.state.value || this.props.secondaryCurrency} style={{ width: 120 }} onChange={this.onSecondValue}>
          {assetsOptions}
        </Select>
        <Input
          className={errorPlace === 'leftPrice' ? 'trade__input trade__input_error' : 'trade__input trade__input_left'}
          onChange={this.changeBordersValue} style={{ width: '250px', marginRight: '10px' }}
          defaultValue={this.props.leftPrice}
          name="leftPrice"
          placeholder="alert to buy"
        />
        <Input
          className={errorPlace === 'rightPrice' ? 'trade__input trade__input_error' : 'trade__input trade__input_right'}
          onChange={this.changeBordersValue} style={{ width: '250px', marginRight: '10px' }}
          defaultValue={this.props.rightPrice}
          name="rightPrice"
          placeholder="alert to sell"
        />
        <Button disabled={!this.props.editing && disabled} onClick={this.saveTradeAlert} loading={loading} type="primary">Сохранить</Button>
        <Popconfirm title="Are you sure delete this task?" onConfirm={this.props.delete.bind(null, this.props.id)} okText="Да" cancelText="Нет">
          <Button type="danger" ghost>Удалить</Button>
        </Popconfirm>
        {error && <Alert showIcon message={error} type="error" />}
      </div>
    )
  }
}
