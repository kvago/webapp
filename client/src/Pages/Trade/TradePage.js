import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';
import gql from 'graphql-tag';
import { Icon, Button, message, Spin } from 'antd';
import Trade from './Trade';
import './TradePage.css';

import BINANCE_TICKERS_PRICE from './trade.json';

const baseAssets = ['BTC', 'ETH', 'USDT', 'BNB'];

class TradePage extends Component {
  static propTypes = {
    userTradePairs: PropTypes.array,
  };
  static defaultProps = {
      userTradePairs: [],
  };
  constructor(props) {
    super(props)
    this.state = {
       tradePairsLimit: 8,
       showEmptyTradeForm: false
    }
  }
  onEdit = async ({id, baseValue, value, leftPrice, rightPrice}) => {
    const editingPair = this.props.userTradePairs.find(pair => pair.id === id);
    const needUpdateDate = editingPair.mainCurrency === baseValue &&
    editingPair.secondaryCurrency === value &&
    editingPair.leftPrice == leftPrice &&
    editingPair.rightPrice == rightPrice;
    if (!needUpdateDate) {
      const result = await this.props.editTradePairMutation({
        variables: {
          id,
          mainCurrency: baseValue,
          secondaryCurrency: value,
          leftPrice,
          rightPrice,
        }
      });
      console.log(result.data.editTradePair)
      if (result.data.editTradePair === null) {
          message.error('Ошибка при сохранении данных');
          return;
      }
      message.success('Успех');
    };
  }
  onSave = async ({baseValue, value, leftPrice, rightPrice}) => {
      const result = await this.props.saveTradePairMutation({
        variables: {
          mainCurrency: baseValue,
          secondaryCurrency: value,
          leftPrice: leftPrice,
          rightPrice: rightPrice,
          stock: 'BINANCE'
        }
      });
      console.log(result.data)
      if (result.data.saveTradePair === null) {
          message.error('Ошибка при сохранении данных');
          return;
      }
      this.setState({
        showEmptyTradeForm: false
      })
      message.success('Успех');
  }
  onDelete = async (id) => {
    const result = await this.props.deleteTradePairMutation({
      variables: {
        id
      }
    });
    if (result.data.deleteTradePair !== null) {
        message.error('Ошибка при сохранении данных');
        return;
    }
    message.success('Успех');
  }
  closeTradeForm = () => {
    this.setState({
      showEmptyTradeForm: false
    });
  }
  addTradePair = () => {
    this.setState({
      showEmptyTradeForm: true
    });
  }
  render() {
    const assets = {
      BTC: [],
      ETH: [],
      USDT: [],
      BNB: []
    };
    BINANCE_TICKERS_PRICE.forEach(pair => {
      if (pair.symbol.includes('USDT')) {
        assets.USDT.push(pair.symbol.slice(0, -4))
      } else if (pair.symbol.slice(-3) ==='ETH') {
        assets.ETH.push(pair.symbol.slice(0, pair.symbol.length - 3))
      } else if (pair.symbol.slice(-3) ==='BTC') {
        assets.BTC.push(pair.symbol.slice(0, pair.symbol.length - 3))
      } else if (pair.symbol.slice(-3) ==='BNB') {
        assets.BNB.push(pair.symbol.slice(0, pair.symbol.length - 3))
      }
    });
    if (this.props.loading) {
      return (
        <div className="trade__page">
          <h2 className="trade__page_header">Загрузка..</h2>
          <div className="trade__page_spinner">
            <Spin size="large" />
          </div>
        </div>
      );
    }
    return (
      <div className="trade__page">
        <h2 className="trade__page_header">Выберите пары для отслеживания</h2>
        {
          this.props.userTradePairs.map(pair => <Trade editing key={pair.id} id={pair.id} mainCurrency={pair.mainCurrency} secondaryCurrency={pair.secondaryCurrency} leftPrice={pair.leftPrice} rightPrice={pair.rightPrice} edit={this.onEdit} save={this.onEdit} delete={this.onDelete} assets={assets} baseAssets={baseAssets} />)
        }
        { this.state.showEmptyTradeForm && <Trade id="0" close={this.closeTradeForm} save={this.onSave} delete={this.closeTradeForm} assets={assets} baseAssets={baseAssets} /> }
        {
          this.props.userTradePairs.length < this.state.tradePairsLimit && <Button disabled={this.state.showEmptyTradeForm} onClick={this.addTradePair} type="dashed" style={{ width: '60%' }}><Icon type="plus" /> Add field</Button>
        }
      </div>
    )
  }
}

const userTradePairs = gql`
  query userTradePairs {
    userTradePairs {
      id
      timestamp
      mainCurrency
      secondaryCurrency
      leftPrice
      rightPrice
    }
  }
`;

const saveTradePair = gql`
  mutation saveTradePair($mainCurrency: String!, $secondaryCurrency: String!, $stock: String, $leftPrice: Float, $rightPrice: Float) {
    saveTradePair(mainCurrency: $mainCurrency, secondaryCurrency: $secondaryCurrency, stock: $stock, leftPrice: $leftPrice, rightPrice: $rightPrice) {
      mainCurrency
      secondaryCurrency
      leftPrice
      rightPrice
    }
  }
`;

const deleteTradePair = gql`
  mutation deleteTradePair($id: String!) {
    deleteTradePair(id: $id) {
      id
    }
  }
`;

const editTradePair = gql`
  mutation changeTradeValues($id: String!, $mainCurrency: String!, $secondaryCurrency: String!, $leftPrice: Float, $rightPrice: Float) {
    changeTradeValues(id: $id, mainCurrency: $mainCurrency, secondaryCurrency: $secondaryCurrency, leftPrice: $leftPrice, rightPrice: $rightPrice) {
      id
      mainCurrency
      secondaryCurrency
      leftPrice
      rightPrice
    }
  }
`;

export default compose(
  graphql(saveTradePair, {
    name: 'saveTradePairMutation',
    options: {
      refetchQueries: [{query: userTradePairs}]
    },
  }),
  graphql(deleteTradePair, {
    name: 'deleteTradePairMutation',
    options: {
      refetchQueries: [{query: userTradePairs}]
    },
  }),
  graphql(editTradePair, {
    name: 'editTradePairMutation',
    options: {
      refetchQueries: [{query: userTradePairs}]
    },
  }),
  graphql(userTradePairs, {
    props: ({ data: { loading, userTradePairs } }) => ({
      loading,
      userTradePairs
    }),
  }),
)(TradePage);


