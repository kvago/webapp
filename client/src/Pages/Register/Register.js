import React from 'react';
import { graphql, compose, withApollo } from 'react-apollo';
import gql from 'graphql-tag';
import { Form, Input, Select, Row, Col, Checkbox, Button } from 'antd/lib/index';
import { GC_AUTH_TOKEN, GC_USER_ID } from '../../constants';
import './Register.css';
const FormItem = Form.Item;
const Option = Select.Option;

class RegistrationForm extends React.Component {
  state = {
    confirmDirty: false
  };
  handleSubmit = (e) => {
    e.preventDefault();
    const sendRegisterQuery = async (name, email, password) => {
        const result = await this.props.createUserMutation({
          variables: {
            name,
            email,
            password
          }
        });
        console.log(result);
        localStorage.setItem(GC_USER_ID, result.data.signInUser.data.user.id);
        localStorage.setItem(GC_AUTH_TOKEN, result.data.signInUser.data.token);
    }
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
          sendRegisterQuery(values.name, values.email, values.password);
          console.log('Received values of form: ', values);
      }
    });
  }
  handleConfirmBlur = (e) => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  }
  checkPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  }
  checkEmail = async (event) => {
    const { value: email } = event.target
    const result = await this.props.client.query({
          variables: { email },
          query: FIND_USER_BY_EMAIL,
    });

    if (result.data.findUserByEmail) {
      this.props.form.setFields({
        email: {
          value: email,
          errors: [new Error('Этот email уже занят')],
        },
      });
    }

  }
  checkConfirm = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  }

  render() {
    const { getFieldDecorator } = this.props.form;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 8,
        },
      },
    };
    const prefixSelector = getFieldDecorator('prefix', {
      initialValue: '86',
    })(
      <Select style={{ width: 70 }}>
        <Option value="86">+86</Option>
        <Option value="87">+87</Option>
      </Select>
    );

    return (
      <div className="register_page">
        <Form onSubmit={this.handleSubmit}>
        <FormItem
          {...formItemLayout}
          label="E-mail"
        >
          {getFieldDecorator('email', {
            rules: [{
              type: 'email', message: 'The input is not valid E-mail!',
            }, {
              required: true, message: 'Please input your E-mail!',
          }],
          })(
            <Input onBlur={this.checkEmail} />
          )}
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="Name"
        >
          {getFieldDecorator('name', {
            rules: [{
              required: true, message: 'Please input your Name!',
            }, {
              validator: this.checkConfirm,
            }],
          })(
            <Input type="name" />
          )}
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="Password"
        >
          {getFieldDecorator('password', {
            rules: [{
              required: true, message: 'Please input your password!',
            }, {
              validator: this.checkConfirm,
            }],
          })(
            <Input type="password" />
          )}
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="Confirm Password"
        >
          {getFieldDecorator('confirm', {
            rules: [{
              required: true, message: 'Please confirm your password!',
            }, {
              validator: this.checkPassword,
            }],
          })(
            <Input type="password" onBlur={this.handleConfirmBlur} />
          )}
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="Phone Number"
        >
          {getFieldDecorator('phone', {
            rules: [{ required: true, message: 'Please input your phone number!' }],
          })(
            <Input addonBefore={prefixSelector} style={{ width: '100%' }} />
          )}
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="Captcha"
          extra="We must make sure that your are a human."
        >
          <Row gutter={8}>
            <Col span={12}>
              {getFieldDecorator('captcha', {
                rules: [{ required: true, message: 'Please input the captcha you got!' }],
              })(
                <Input />
              )}
            </Col>
            <Col span={12}>
              <Button>Get captcha</Button>
            </Col>
          </Row>
        </FormItem>
        <FormItem {...tailFormItemLayout}>
          {getFieldDecorator('agreement', {
            valuePropName: 'checked',
          })(
            <Checkbox>I have read the <a href="">agreement</a></Checkbox>
          )}
        </FormItem>
        <FormItem {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit">Register</Button>
        </FormItem>
      </Form>
    </div>
    );
  }
}

const FIND_USER_BY_EMAIL = gql`
query ($email: String!) {
    findUserByEmail(
        email: $email
    ) {
        email
    }
}
`;

const CREATE_USER_MUTATION = gql`
mutation CreateUserMutation($name: String!, $email: String!, $password: String!) {
  createUser(
    name: $name,
    authProvider: {
      email: {
        email: $email,
        password: $password
      }
    }
  ) {
    id
  }
  signInUser(email: {
    email: $email,
    password: $password
  }) {
    message
    status
    data {
      token
      user {
        id
        name
      }
    }
  }
}
`;

export default compose(
    Form.create(),
    graphql(CREATE_USER_MUTATION, {
      options: {
        errorPolicy: 'ignore'
      },
      name: 'createUserMutation'
    })
)(withApollo(RegistrationForm));
