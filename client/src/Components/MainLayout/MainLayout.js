import React from 'react';
import { Route, NavLink } from "react-router-dom";
import { Layout, Menu, Icon } from 'antd/lib/index';
import TradePage from '../../Pages/Trade/TradePage';
import SettingsPage from '../../Pages/Settings/SettingsPage';
import ProfilePage from '../../Pages/Profile/ProfilePage';
import './MainLayout.css';

const { Header, Sider, Content } = Layout;

const routes = [
  {
    path: "/main",
    exact: true,
    main: () => <TradePage />
  },
  {
    path: "/main/settings",
    exact: true,
    main: () => <SettingsPage />
  },
  {
    path: "/main/profile",
    exact: true,
    main: () => <ProfilePage />
  }
];

const contentStyles = {
  margin: '24px 16px',
  padding: 24,
  background: '#fff',
  minHeight: 280
}

export default class MainLayout extends React.Component {
    state = {
        collapsed: false,
    };
    toggleSidebar = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    }

    render() {
      const { collapsed } = this.state
        return (
            <Layout className="main_layout">
                <Sider
                    trigger={null}
                    collapsible
                    collapsed={collapsed}
                >
                    <div className="logo" />
                    <Menu theme="dark" mode="inline" selectedKeys={[this.props.location.pathname]} defaultSelectedKeys={['user']}>
                        <Menu.Item key="1">
                            <Icon type="user" />
                            <NavLink activeClassName="selected" to="/main">Home</NavLink>
                        </Menu.Item>
                        <Menu.Item key="2">
                            <Icon type="video-camera" />
                            <NavLink activeClassName="selected" to="/main/settings">Settings</NavLink>
                        </Menu.Item>
                        <Menu.Item key="3">
                            <Icon type="upload" />
                            <NavLink activeClassName="selected" to="/main/profile">Profile</NavLink>
                        </Menu.Item>
                    </Menu>
                </Sider>
                <Layout>
                    <Header style={{ background: '#fff', padding: 0 }}>
                        <Icon
                            className="trigger"
                            type={collapsed ? 'menu-unfold' : 'menu-fold'}
                            onClick={this.toggleSidebar}
                        />
                    </Header>
                    <Content style={contentStyles}>
                        {
                            routes.map(({path, exact, main }) => (
                                <Route
                                    key={path}
                                    path={path}
                                    exact={exact}
                                    component={main}
                                />
                            ))
                        }
                    </Content>
                </Layout>
            </Layout>
        );
    }
}
