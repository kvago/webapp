import React from 'react';
import Header  from './HeaderComponent'

import { TokenContext } from '../../Context/TokenContext/token-context'
import { UserContext } from '../../Context/UserContext/user-context'

export const HeaderWrapper = () => {
    return (
        <UserContext.Consumer>
            {
                (({ user, setUser }) => (
                    <TokenContext.Consumer>
                        {
                            ({ token, clearToken }) => (
                                <Header
                                    user={user}
                                    setUser={setUser}
                                    token={token}
                                    logout={clearToken}
                                />
                            )
                        }
                    </TokenContext.Consumer>
                ))
            }
        </UserContext.Consumer>
    );
}
