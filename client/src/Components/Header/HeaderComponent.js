import React, { useEffect } from "react";
import { NavLink } from "react-router-dom";
import { graphql } from "react-apollo";
import gql from "graphql-tag";

import './Header.css';

const HeaderPrivateRoutes = ({ user: { name }, logout }) => (
  <ul>
    <li>
      <NavLink exact activeClassName="selected" to='/'>Home</NavLink>
    </li>
    <li>
      <NavLink activeClassName="selected" to='/main'>main</NavLink>
    </li>
    <li>
      {name}
    </li>
    <li>
      <button onClick={logout}>Logout</button>
    </li>
  </ul>
)

const HeaderBasicRoutes = () => (
  <ul>
    <li>
      <NavLink exact activeClassName="selected" to='/'>Home</NavLink>
    </li>
    <li>
      <NavLink activeClassName="selected" to='/register'>register</NavLink>
    </li>
    <li>
      <NavLink activeClassName="selected" to='/login'>login</NavLink>
    </li>
  </ul>
)

const Header = ({ user, setUser, logout, userProfile }) => {

  useEffect(() => {
    if (userProfile) {
      const { id, name } = userProfile
      setUser({ id, name })
    }
  }, [userProfile]);

  const displayUser = Object.keys(user).length > 0 ? user : userProfile
  return (
    <header>
      <div className="main_logo">logo</div>
      <div className="header_navigation">
        {
          displayUser ? (
            <HeaderPrivateRoutes
              user={displayUser}
              logout={logout}
            />
            ) : <HeaderBasicRoutes />
        }
      </div>
    </header>
  )
}

const FIND_CURRENT_USER = gql`
query CurrentUserProfile($token: String) {
  userProfile (token: $token) {
      name
      email
      registrationDate
  }
}
`;

export default graphql(FIND_CURRENT_USER, {
  options: ({ token }) => ({
    variables: { token },
    errorPolicy: 'ignore',
  }),
  props: ({ data: { loading, userProfile } }) => ({
    loading,
    userProfile
  })
})(Header);
