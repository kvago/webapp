import React, { Component } from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import { TokenContext } from './Context/TokenContext/token-context'
import { UserContext } from './Context/UserContext/user-context'
import { GC_AUTH_TOKEN } from './constants';
import PrivateRoute from './PrivatedRoute'
import MainLayout from './Components/MainLayout/MainLayout';
import Login from './Pages/Login/LoginWrapper';
import Home from './Pages/Home/Home';
import { HeaderWrapper } from './Components/Header/HeaderWrapper';
import Footer from './Components/Footer/Footer';
import Register from './Pages/Register/Register';
import Test from './Test';
import NotFound from './Pages/NotFound/NotFound';
import 'normalize.css';

import './App.css';

class App extends Component {
    constructor(props) {
        super(props);

        this.clearToken = () => {
            this.setState(() => ({
                token: '',
                user: {}
            }), () => this.logout() );
        };

        this.setUser = ({ id, name }) => {
            this.setState(() => ({
                user: {
                    id,
                    name
                }
            }))
        }

        this.state = {
            user: {},
            token: localStorage.getItem(GC_AUTH_TOKEN),
            clearToken: this.clearToken,
            setUser: this.setUser
        };
    }

    logout = () => {
        localStorage.setItem(GC_AUTH_TOKEN, '')
        this.props.history.push('/');
    }

    render() {
        const { user } = this.state
        return (
            <UserContext.Provider value={this.state}>
            <TokenContext.Provider value={this.state}>
                <div className="app_container">
                    <HeaderWrapper />
                    <Switch>
                        <Route exact path='/' component={Home} />
                        <Route path='/login' component={Login} />
                        <Route path='/register' component={Register} />
                        <PrivateRoute path='/main' user={user} component={MainLayout} />
                        <Route path='/test' component={Test} />
                        <Route component={NotFound} />
                    </Switch>
                    <Footer />
                  </div>
            </TokenContext.Provider>
            </UserContext.Provider>
        );
    }
}

export default withRouter(App);
